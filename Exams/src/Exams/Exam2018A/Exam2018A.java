package Exams.Exam2018A;

public class Exam2018A {
    // Tests
    public static void main(String[] args) {
        System.out.println("Q1");
        System.out.println(square(246) ? "True" : "False");
        System.out.println(square(200) ? "True" : "False");

        System.out.println("Q2");
        System.out.println(reduce("aaabbccccxxxyzza"));
        System.out.println(reduce("abcdeb"));

        System.out.println("Q3");
        System.out.println(symmetric(new int[][]{}, new int[][]{}) ? "True" : "False");
        System.out.println(symmetric(new int[][]{{5}}, new int[][]{}) ? "True" : "False");
        System.out.println(symmetric(new int[][]{{1,2,3,4},{5,6,7},{8,9,10,11,12},{13,14,15,16},{17,18,19}}
                , new int[][]{{17,18,19},{13,14,15,16},{8,9,10,11,12},{5,6,7},{1,2,3,4}}) ? "True" : "False");
        System.out.println(symmetric(new int[][]{{1,2,3,4},{5,6,7},{8,9,10,11,12},{13,14,15,16},{17,18,19}}
                , new int[][]{{4,3,2,1},{7,6,5},{12,11,10,9,8},{16,15,14,13},{19,18,17}}) ? "True" : "False");
        System.out.println(symmetric(new int[][]{{1,2,3,4},{5,6,7},{8,9,11,12},{13,14,15,16},{17,18,19}}
                , new int[][]{{4,3,2,1},{7,6,5},{12,11,10,9,8},{16,15,14,13},{19,18,17}}) ? "True" : "False");
        System.out.println(symmetric(new int[][]{{1,2,3,4},{5,6,7},{8,9,10,11,12},{13,14,15,16},{17,18,19}}
                , new int[][]{{1,2,3,4},{5,6,7},{8,9,10,11,12},{13,14,15,16},{17,18,19}}) ? "True" : "False");

        System.out.println("Q4");
        Line l1 = new Line(0, 0, 5, 5);
        Line l2 = new Line(l1);
        System.out.println(l1.length());
        System.out.println(l2.length());
        System.out.println(l2.on(0, 0) ? "True" : "False");
        System.out.println(l2.on(5, 5) ? "True" : "False");
        System.out.println(l2.on(3, 3) ? "True" : "False");
        System.out.println(l2.on(3, 4) ? "True" : "False");
        System.out.println(l2.on(6, 6) ? "True" : "False");

        System.out.println("Q5");
        MyLinkedList list = new MyLinkedList();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        System.out.print(list.toString() + " - ");
        System.out.println(list.swap(-1, 5) ? "True" : "False");
        System.out.println(list.toString());

        list = new MyLinkedList();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        System.out.print(list.toString() + " - ");
        System.out.println(list.swap(1, 9) ? "True" : "False");
        System.out.println(list.toString());

        list = new MyLinkedList();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        System.out.print(list.toString() + " - ");
        System.out.println(list.swap(0, 5) ? "True" : "False");
        System.out.println(list.toString());

        list = new MyLinkedList();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        System.out.print(list.toString() + " - ");
        System.out.println(list.swap(0, 4) ? "True" : "False");
        System.out.println(list.toString());

        list = new MyLinkedList();
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        System.out.print(list.toString() + " - ");
        System.out.println(list.swap(2, 4) ? "True" : "False");
        System.out.println(list.toString());
    }

    // Q1
    public static boolean square(int a) {
        if (a == 1) {
            return true;
        }
        int sum = 0;
        for (int i = 1; i <= a; i++) {
            if (a % i == 0) {
                sum += i * i;
            }
        }
        int sq = (int) Math.sqrt(sum);

        return sum == sq * sq;
    }

    // Q2
    public static String reduce(String s) {
        StringBuilder ret = new StringBuilder("");

        if (s.length() == 0) {
            return "";
        }

        char first = s.charAt(0);
        ret.append(first);
        for (int i = 1; i < s.length(); i++) {
            char tv = s.charAt(i);
            if (first != tv) {
                ret.append(tv);
                first = tv;
            }
        }

        return ret.toString();
    }

    // Q3
    public static boolean symmetric(int[][] a, int[][] b) {
        if (a.length != b.length) {
            return false;
        }
        if (a.length == 0) {
            return true;
        }

        int h = a.length;
        boolean flag = true;
        for (int y = 0; y < h && flag; y++) {
            if (a[y].length != b[y].length) {
                flag = false;
            } else {
                for (int x = 0; x < a[y].length && flag; x++) {
                    flag = (a[y][x] == b[y][a[y].length - x - 1]);
                }
            }
        }
        if (flag) {
            return true;
        }

        flag = true;
        for (int y = 0; y < h && flag; y++) {
            if (a[y].length != b[h - y - 1].length) {
                flag = false;
            } else {
                for (int x = 0; x < a[y].length && flag; x++) {
                    flag = (a[y][x] == b[h - y - 1][x]);
                }
            }
        }
        return flag;
    }

    // Q4
    // In Line.java

    // Q5
    // In MyLinkedList.java
}
