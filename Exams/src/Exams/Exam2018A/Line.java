package Exams.Exam2018A;

public class Line {
    private int x1, y1;
    private int x2, y2;

    public Line(int x1, int y1, int x2, int y2) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
    }

    public Line(Line src) {
        this.x1 = src.x1;
        this.y1 = src.y1;
        this.x2 = src.x2;
        this.y2 = src.y2;
    }

    public double length() {
        return Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
    }

    public boolean on(int x, int y) {
        Line l1 = new Line(x1, y1, x, y);
        Line l2 = new Line(x, y, x2, y2);
        return l1.length() + l2.length() == this.length();
    }
}
