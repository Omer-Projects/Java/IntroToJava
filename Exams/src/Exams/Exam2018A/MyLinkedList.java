package Exams.Exam2018A;

public class MyLinkedList {
    Node head;
    int size;

    public MyLinkedList(){
        head = null;
        size = 0;
    }

    public void add(String data){
        if (head == null){
            head = new Node(data);
        }
        else{
            Node n = head;
            while(n.next != null){
                n = n.next;
            }
            n.next = new Node(data);
        }
        size++;
    }

    public String toString(){
        String ans = "[";
        if (head == null) ans = "[]";
        else {
            Node n = head;
            while(n.next != null){
                ans = ans + n.data + ", ";
                n = n.next;
            }
            ans = ans + n.data +"]";
        }
        return ans;
    }

    public boolean swap(int i, int j) {
        if (i < 0 || j < 0) {
            return false;
        }

        if (i > j) {
            int temp = i;
            i = j;
            j = temp;
        }

        int iOrigin = i;

        j = j - i;

        Node node1 = head;
        Node node2;

        while (i > 1) {
            if (node1.next == null) {
                return false;
            }
            node1 = node1.next;
            i--;
        }

        if (j == 0) {
            return false;
        }

        if (node1.next == null) {
            return false;
        }

        node2 = node1.next;
        while (j > 1) {
            if (node2.next == null) {
                return false;
            }
            node2 = node2.next;
            j--;
        }

        if (node2.next == null) {
            return false;
        }

        // swap
        Node nodeI = node1.next;
        Node nodeJ = node2.next;
        if (iOrigin == 0) {
            nodeI = head;

            Node tempNode = head.next;
            head.next = nodeJ.next;
            nodeJ.next = tempNode;

            node2.next = head;
            head = nodeJ;

        } else {
            Node tempNode = node2.next;
            node2.next = node1.next;
            node1.next = tempNode;
        }

        Node tempNode = nodeI.next;
        nodeI.next = nodeJ.next;
        nodeJ.next = tempNode;

        return true;
    }
}