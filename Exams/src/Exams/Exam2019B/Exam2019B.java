package Exams.Exam2019B;

public class Exam2019B {

    // Tests
    public static void main(String[] args) {
        System.out.println("Q1");
        System.out.println(sphenic(2*3*5) ? "True" : "False");
        System.out.println(sphenic(5*7*13) ? "True" : "False");
        System.out.println(sphenic(101*7*13) ? "True" : "False");
        System.out.println(sphenic(2*2) ? "True" : "False");
        System.out.println(sphenic(1) ? "True" : "False");
        System.out.println(sphenic(7*13) ? "True" : "False");
        System.out.println(sphenic(13*13*7) ? "True" : "False");
        System.out.println(sphenic(2*2*2) ? "True" : "False");
        System.out.println(sphenic(2) ? "True" : "False");
        System.out.println(sphenic(64) ? "True" : "False");

        System.out.println("Q2");
        System.out.println(longestCommonSequence("atatczx", "computation"));
        System.out.println(longestCommonSequence("", "a"));
        System.out.println(longestCommonSequence("computation", "coutabtion"));

        System.out.println("Q3");
        System.out.println(isMagicSquare(new int[][] {{2,9,4},{7,5,3},{6,1,8}}) ? "True" : "False");

        System.out.println("Q4");
        PhoneNumber phone1 = new PhoneNumber("05", 8665643);
        PhoneNumber phone2 = new PhoneNumber(phone1);
        PhoneNumber phone3 = new PhoneNumber("0a", 8665643);
        PhoneNumber phone4 = new PhoneNumber("0", 8665643);
        PhoneNumber phone5 = new PhoneNumber("05", 18665643);
        PhoneNumber phone6 = new PhoneNumber("05", -5643);
        PhoneNumber phone7 = new PhoneNumber("05", -8665643);
        PhoneNumber phone8 = new PhoneNumber("05", 5643);

        System.out.println(phone1.checkInput() ? "True" : "False");
        System.out.println(phone2.checkInput() ? "True" : "False");
        System.out.println(phone3.checkInput() ? "True" : "False");
        System.out.println(phone4.checkInput() ? "True" : "False");
        System.out.println(phone5.checkInput() ? "True" : "False");
        System.out.println(phone6.checkInput() ? "True" : "False");
        System.out.println(phone7.checkInput() ? "True" : "False");
        System.out.println(phone8.checkInput() ? "True" : "False");

        System.out.println(phone1.toString());
        System.out.println(phone2.toString());
        System.out.println(phone8.toString());

        System.out.println("Q5");
        MyLinkedList list1 = new MyLinkedList();
        list1.add(1);
        list1.add(6);
        list1.add(2);
        list1.add(3);
        list1.add(6);
        list1.add(1);
        list1.add(1);
        list1.add(4);
        list1.add(9);
        list1.add(4);
        System.out.print(list1.toString() + " - ");
        list1.removeDup();
        System.out.println(list1.toString());
    }

    // Q1
    public static boolean sphenic(int n) {
        int count = 0;
        int num = n;
        for (int i = 2; i < n; i++) {
            if (num % i == 0) {
                num /= i;
                if (num % i == 0) {
                    return false;
                }
                count++;
                if (count == 3) {
                    return num == 1;
                }
            }
        }
        return false;
    }

    // Q2
    public static String longestCommonSequence(String s, String t) {
        if (s.equals("") || t.equals("")) {
            return "";
        }

        String max = "";
        for (int i = 0; i + max.length() < s.length(); i++) {
            int j = i + max.length();
            String substring = s.substring(i, j+1);
            if (t.contains(substring)) {
                j++;
                while (j < s.length() && t.contains(substring+s.charAt(j))) {
                    substring += s.charAt(j);
                    j++;
                }
                max = substring;
            }
        }
        return max;
    }

    // Q3
    public static boolean isMagicSquare(int[][] a) {
        if (a.length == 0)
            return true;
        if (a.length != a[0].length) {
            return false;
        }

        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i][i];
        }

        for (int i = 0; i < a.length; i++) {
            int s1 = 0;
            int s2 = 0;
            for (int j = 0; j < a.length; j++) {
                s1 += a[i][j];
                s2 += a[j][i];
            }
            if (s1 != sum || s2 != sum) {
                return false;
            }
        }

        int s = 0;
        for (int i = 0; i < a.length; i++) {
            s += a[i][a.length-i-1];
        }

        return s == sum;
    }

    // Q4
    // In PhoneNumber.java

    // Q5
    // In MyLinkedList.java
}
