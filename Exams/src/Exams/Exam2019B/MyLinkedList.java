package Exams.Exam2019B;

public class MyLinkedList {
    private Node head;
    private int size;
    public MyLinkedList(){
        head = null;
        size = 0;
    }
    public void add(int data){
        if (head == null){
            head = new Node(data);
        }
        else{
            Node n = head;
            while(n.next != null){
                n = n.next;
            }
            n.next = new Node(data);
        }
        size++;
    }
    public String toString(){
        String ans = "[";
        if (head == null) ans = "[]";
        else {
            Node n = head;
            while(n.next != null){
                ans = ans + n.getData() + ", ";
                n = n.next;
            }
            ans = ans + n.getData() +"]";
        }
        return ans;
    }

    public void removeDup() {
        if (head == null || head.next == null) {
            return;
        }
        Node node = head;
        while (node.next != null) {
            Node before = node;
            node = node.next;

            Node back = head;
            boolean exist = false;
            while (!exist && back != node) {
                exist = back.getData() == node.getData();
                back = back.next;
            }

            if (exist) {
                before.next = node.next;
                node = before;
                size--;
            }
        }
    }
}