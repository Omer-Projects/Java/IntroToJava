package Exams.Exam2019B;

public class PhoneNumber {
    private String city;
    private int number;

    public PhoneNumber(String city, int number) {
        this.city = city;
        this.number = number;
    }

    public PhoneNumber(PhoneNumber src) {
        this.city = src.city;
        this.number = src.number;
    }

    public boolean checkInput() {
        if (city.length() != 2)
            return false;

        if (city.charAt(0) < '0' || city.charAt(0) > '9')
            return false;
        if (city.charAt(1) < '0' || city.charAt(1) > '9')
            return false;

        return 0 <= number && number < 10000000;
    }

    public String toString() {
        if (number == 0) {
            return city + "-0";
        }

        String ret = "";
        int num = number;
        while (num > 0) {
            ret = (char)(num % 10 + (int)'0') + ret;
            num /= 10;
        }

        return city + "-" + ret;
    }
}
