package Exams.Exam2019C;

public class Exam2019C {

    // Tests
    public static void main(String[] args) {
        System.out.println("Q1");
        System.out.println(MarsennePrime(3) ? "True" : "False");
        System.out.println(MarsennePrime(7) ? "True" : "False");
        System.out.println(MarsennePrime(127) ? "True" : "False");
        System.out.println(MarsennePrime(1) ? "True" : "False");
        System.out.println(MarsennePrime(9) ? "True" : "False");
        System.out.println(MarsennePrime(8) ? "True" : "False");
        System.out.println(MarsennePrime(15) ? "True" : "False");
        System.out.println(MarsennePrime(63) ? "True" : "False");
        System.out.println(MarsennePrime(255) ? "True" : "False");

        System.out.println("Q2");
        System.out.println(single("abcd9cb"));
        System.out.println(single("abracadabra"));

        System.out.println("Q3");
        System.out.println(horizontal(new int[][] {{7,10,8},{2,-3,0},{4,1,2},{5,6,6}}) ? "True" : "False");

        System.out.println("Q4");
        System.out.println("This question not need answer!");

        System.out.println("Q5");
        MyLinkedList list1 = new MyLinkedList();
        MyLinkedList list2 = new MyLinkedList();
        list2.add("1");
        MyLinkedList list3 = new MyLinkedList();
        list3.add("1");
        list3.add("2");
        MyLinkedList list4 = new MyLinkedList();
        list4.add("1");
        list4.add("2");
        list4.add("3");
        MyLinkedList list5 = new MyLinkedList();
        list5.add("1");
        list5.add("2");
        list5.add("3");
        list5.add("4");
        list5.add("5");
        list5.add("6");
        MyLinkedList list6 = new MyLinkedList();
        list6.add("1");
        list6.add("2");
        list6.add("3");
        list6.add("4");
        list6.add("5");
        list6.add("6");
        list6.add("7");
        list6.add("8");
        list6.add("9");
        System.out.print(list1 + " - ");
        list1.invert();
        System.out.println(list1);
        System.out.print(list2 + " - ");
        list2.invert();
        System.out.println(list2);
        System.out.print(list3 + " - ");
        list3.invert();
        System.out.println(list3);
        System.out.print(list4 + " - ");
        list4.invert();
        System.out.println(list4);
        System.out.print(list5 + " - ");
        list5.invert();
        System.out.println(list5);
        System.out.print(list6 + " - ");
        list6.invert();
        System.out.println(list6);
    }

    // Q1
    public static boolean MarsennePrime(int n) {
        if (n < 3)
            return false;

        int back = n + 1;
        while (back % 2 == 0) {
            back /= 2;
        }
        if (back != 1) {
            return false;
        }

        for (int i = 3; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }

        return true;
    }

    // Q2
    public static String single(String s) {
        StringBuilder ret = new StringBuilder("");

        for (int i = 0; i < s.length(); i++) {
            char tv = s.charAt(i);
            if (s.length() - 1 == s.replace("" + tv, "").length()) {
                ret.append(tv);
            }
        }
        return ret.toString();
    }

    // Q3
    public static boolean horizontal(int[][] a) {
        int h = a.length;
        if (h == 0)
            return true;
        if (h % 2 != 0)
            return false;

        int w = a[0].length;
        for (int x = 0; x < w; x++) {
            int sumUp = 0;
            int sumDown = 0;
            for (int y = 0; y < h / 2; y++) {
                sumUp += a[y][x];
                sumDown += a[y+y/h][x];
            }
            if (sumDown != sumUp)
                return false;
        }
        return true;
    }

    // Q4
    // In Student.java

    // Q5
    // In MyLinkedList.java
}
