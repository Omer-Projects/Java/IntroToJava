package Exams.Exam2019C;

public class MyLinkedList {
    private Node head;
    private int size;
    public MyLinkedList(){
        head = null;
        size = 0;
    }
    public void add(String data){
        if (head == null){
            head = new Node(data);
        }
        else{
            Node n = head;
            while(n.next != null){
                n = n.next;
            }
            n.next = new Node(data);
        }
        size++;
    }
    public String toString(){
        String ans = "[";
        if (head == null) ans = "[]";
        else {
            Node n = head;
            while(n.next != null){
                ans = ans + n + ", ";
                n = n.next;
            }
            ans = ans + n +"]";
        }
        return ans;
    }

    public boolean invert() {
        if (head == null || head.next == null) {
            return true;
        }

        Node newHead = head;
        Node back = null;
        while (newHead.next != null) {
            back = newHead;
            newHead = newHead.next;
        }
        back.next = null;

        Node add = newHead;
        while (back != head) {
            Node node = head;
            while (node.next != null) {
                back = node;
                node = node.next;
            }
            back.next = null;
            add.next = node;
            add = add.next;
        }

        head.next = null;
        add.next = head;
        head = newHead;
        return true;
    }
}