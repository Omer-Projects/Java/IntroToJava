package Exams.Exam2019C;

public class Node {
    private String data;
    Node next;
    public Node(String data){
        this.data = data;
        this.next = null;
    }
    public String toString(){
        return data;
    }
}
