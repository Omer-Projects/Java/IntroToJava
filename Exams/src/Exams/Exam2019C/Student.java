package Exams.Exam2019C;

public class Student {

    private String name;
    private int age;
    private float average;

    Student(String name, int age, float average) {
        this.name = name;
        this.age = age;
        this.average = average;
    }

    Student(String name, int age) {
        this.name = name;
        this.age = age;
        this.average = 0;
    }

    Student(Student src) {
        this.name = src.name + "";
        this.age = src.age;
        this.average = src.average;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public float getAverage() {
        return average;
    }

    public void setAverage(float average) {
        this.average = average;
    }

    public String toString() {
        return "{Name: " + name + ", Age: " + age + ", Average: " + average + "}";
    }

    public boolean equals(Student other) {
        return this.age == other.age && this.average == other.average;
    }
}
