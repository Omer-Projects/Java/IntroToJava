package Exams.Exam2020B;

public class Exam2020B {

    // Tests
    public static void main(String[] args) {
        System.out.println("Q1");
        System.out.println(semi(4) ? "True" : "False");
        System.out.println(semi(6) ? "True" : "False");
        System.out.println(semi(15) ? "True" : "False");
        System.out.println(semi(5) ? "True" : "False");
        System.out.println(semi(12) ? "True" : "False");
        System.out.println(semi(30) ? "True" : "False");

        System.out.println("Q2");
        System.out.println(findMaxNum("93&*ab1234crt70"));
        System.out.println(findMaxNum("8ab(55c#r9t"));
        System.out.println(findMaxNum("hello"));

        System.out.println("Q3");
        System.out.println(diagonal(new int[][]{{1,2,3,4}, {5,1,2,3}, {9,5,1,2}}) ? "True" : "False");
        System.out.println(diagonal(new int[][]{{1,2}, {2,2}}) ? "True" : "False");

        System.out.println("Q4");
        Segmen s = new Segmen(new Point(0,0), new Point(2,4));
        System.out.println(s.distance());
        System.out.println(s.isOn(new Point(0,0)) ? "True" : "False");
        System.out.println(s.isOn(new Point(2,4)) ? "True" : "False");
        System.out.println(s.isOn(new Point(1,2)) ? "True" : "False");
        System.out.println(s.isOn(new Point(2,1)) ? "True" : "False");

        System.out.println("Q5");
        Node head1 = new Node(-2);
        Node n12 = new Node(3);
        Node n13 = new Node(100);
        Node n14 = new Node(9);
        Node n15 = new Node(-30);
        Node n16 = new Node(11);
        Node n17 = new Node(5);
        Node n18 = new Node(3);
        head1.setNext(n12);
        n12.setNext(n13);
        n13.setNext(n14);
        n14.setNext(n15);
        n15.setNext(n16);
        n16.setNext(n17);
        n17.setNext(n18);
        n18.setNext(n15);

        Node cy1 = cycle(head1);
        System.out.println((cy1 == null) ? "null" : cy1.getData());

        Node head2 = new Node(-2);
        Node n22 = new Node(3);
        Node n23 = new Node(100);
        Node n24 = new Node(9);
        Node n25 = new Node(-30);
        Node n26 = new Node(11);
        Node n27 = new Node(5);
        head2.setNext(n22);
        n22.setNext(n23);
        n23.setNext(n24);
        n24.setNext(n25);
        n25.setNext(n26);
        n26.setNext(n27);
        n27.setNext(null);

        Node cy2 = cycle(head2);
        System.out.println((cy2 == null) ? "null" : cy2.getData());
    }

    // Q1
    public static boolean semi(int n) {
        for (int a = 2; a < n; a++)
        {
            if (n % a == 0) {
                int b = n / a;
                for (int i = 2; i < a; i++) {
                    if (a % i == 0 || b % i == 0) {
                        return false;
                    }
                }
                for (int i = a; i < b; i++) {
                    if (b % i == 0) {
                        return false;
                    }
                }
                return true;
            }
        }

        return false;
    }

    // Q2
    public static int findMaxNum(String str) {
        int maxValue = -1;
        String number = "";
        for (int i = 0; i < str.length(); i++) {
            char tv = str.charAt(i);
            if (tv >= '0' && tv <= '9') {
                number = number + tv;
            } else {
                if (!number.equals("")) {
                    int value = Integer.parseInt(number);
                    if (value > maxValue) {
                        maxValue = value;
                    }
                    number = "";
                }
            }
        }
        if (!number.equals("")) {
            int value = Integer.parseInt(number);
            if (value > maxValue) {
                maxValue = value;
            }
            number = "";
        }
        return maxValue;
    }

    // Q3
    public static boolean diagonal(int[][] a) {
        if (a.length == 0 || a.length == 1 || a[0].length == 1)
            return true;

        int h = a.length;
        int w = a[0].length;

        for (int x = 1-h; x < w; x++) {
            int i = x;
            int j = 0;
            if (x < 0) {
                i = 0;
                j = x * -1;
            }
            int value = a[j][i];
            while (j < h && i < w) {
                if (a[j][i] != value) {
                    return false;
                }
                i++;
                j++;
            }
        }
        return true;
    }

    // Q 4
    // in Segmen.java

    // Q 5
    public static Node cycle(Node p) {
        if (p == null || p.getNext() == null)
            return null;
        if (p.getNext() == p)
            return p;

        Node head = p;

        Node node = head;

        while (node.getNext() != null)
        {
            Node check = head;
            Node next = node.getNext();
            if (head == next) {
                return head;
            }
            if (node == next) {
                return node;
            }
            while (check != node) {
                if (check == next) {
                    return check;
                }
                check = check.getNext();
            }

            node = next;
        }

        return null;
    }
}
