package Exams.Exam2020B;

// Q 4
public class Segmen {
    public Point a;
    public Point b;

    Segmen(Point a, Point b) {
        this.a = a;
        this.b = b;
    }

    Segmen(Segmen src) {
        this.a = new Point(src.a.getX(), src.a.getY());
        this.b = new Point(src.b.getX(), src.b.getY());
    }

    double distance() {
        double calc = Math.pow(b.getX() - a.getX(), 2);
        calc += Math.pow(b.getY() - a.getY(), 2);
        return Math.sqrt(calc);
    }

    boolean isOn(Point point) {
        Segmen s1 = new Segmen(a, point);
        Segmen s2 = new Segmen(point, b);

        return (s1.distance() + s2.distance()) == distance();
    }
}