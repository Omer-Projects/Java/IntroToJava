package com.company.lesson1;

import com.company.lesson1.e1.E1;
import com.company.lesson1.e2.E2;
import com.company.lesson1.e3.E3;
import com.company.lesson1.e4.E4;
import com.company.lesson1.e5.E5;
import com.company.lesson1.e6.E6;
import com.company.lesson1.e7.E7;
import com.company.lesson1.e8.E8;
import com.company.lesson1.e9.E9;
import com.company.lesson1.e10.E10;
import com.company.lesson1.e11.E11;
import com.company.lesson1.e12.E12;

import java.util.Scanner;

public class Main {

    /*
    * Lesson 1
    *
    * Select and run the exercises.
    * */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Choose a exercise: ");
        int exerciseNumber = scanner.nextInt();
        System.out.println();

        switch (exerciseNumber)
        {
            case 1: E1.main(args); break;
            case 2: E2.main(args); break;
            case 3: E3.main(args); break;
            case 4: E4.main(args); break;
            case 5: E5.main(args); break;
            case 6: E6.main(args); break;
            case 7: E7.main(args); break;
            case 8: E8.main(args); break;
            case 9: E9.main(args); break;
            case 10: E10.main(args); break;
            case 11: E11.main(args); break;
            case 12: E12.main(args); break;
        }
    }
}
