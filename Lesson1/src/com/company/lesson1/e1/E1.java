package com.company.lesson1.e1;

import java.util.Scanner;

public class E1 {

    // Take two numbers and swith them
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("X: ");
        int x = scanner.nextInt();
        System.out.print("Y: ");
        int y = scanner.nextInt();
        System.out.println();

        System.out.println("X: " + x);
        System.out.println("Y: " + y);
        System.out.println();

        int temp = x;
        x = y;
        y = temp;
        System.out.println("X: " + x);
        System.out.println("Y: " + y);
    }
}
