package com.company.lesson1.e10;

import java.util.Scanner;

public class E10 {

    // Get time by seconds and print it as the Time format HH:MM:SS
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Get time in day by seconds: ");
        int time = scanner.nextInt();

        int seconds = time % 60;
        int minutes = time % 360 / 60;
        int hours = time / 60;

        System.out.println("Time: " + hours + ":" + minutes + ":"+ seconds);
    }
}
