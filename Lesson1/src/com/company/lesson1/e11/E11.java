package com.company.lesson1.e11;

import java.util.Scanner;

public class E11 {

    // Take a Radius of circle and print his Area and Scopeץ
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        final float PI = 3.1415f;

        System.out.print("Radius: ");
        float radius = scanner.nextFloat();

        float area = PI * radius * radius;
        float scope = 2 * PI * radius;

        System.out.println("Area: " + area);
        System.out.println("Scope: " + scope);
    }
}
