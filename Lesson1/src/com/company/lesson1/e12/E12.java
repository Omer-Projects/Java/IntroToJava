package com.company.lesson1.e12;

import java.util.Scanner;

public class E12 {

    // Print min and max value of types
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("int MIN_VALUE: " + Integer.MIN_VALUE);
        System.out.println("    MAX_VALUE: " + Integer.MAX_VALUE);
        System.out.println();

        System.out.println("short MIN_VALUE: " + Short.MIN_VALUE);
        System.out.println("      MAX_VALUE: " + Short.MAX_VALUE);
        System.out.println();

        System.out.println("long MIN_VALUE: " + Long.MIN_VALUE);
        System.out.println("     MAX_VALUE: " + Long.MAX_VALUE);
        System.out.println();

        System.out.println("double MIN_VALUE: " + Double.MIN_VALUE);
        System.out.println("       MAX_VALUE: " + Double.MAX_VALUE);
        System.out.println();

        System.out.println("float MIN_VALUE: " + Float.MIN_VALUE);
        System.out.println("      MAX_VALUE: " + Float.MIN_VALUE);
        System.out.println();
    }
}
