package com.company.lesson1.e2;

import java.util.Scanner;

public class E2 {

    //
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("X: ");
        int x = scanner.nextInt();
        System.out.print("Y: ");
        int y = scanner.nextInt();
        System.out.print("Z: ");
        int z = scanner.nextInt();
        System.out.print("U: ");
        int u = scanner.nextInt();
        System.out.println();

        int temp = x;
        x = y;
        y = z;
        z = u;
        u = temp;

        System.out.println("X: " + x);
        System.out.println("Y: " + y);
        System.out.println("Z: " + z);
        System.out.println("U: " + u);
        System.out.println();
    }
}
