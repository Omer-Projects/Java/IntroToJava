package com.company.lesson1.e3;

import java.util.Scanner;

public class E3 {

    // Take two numbers and print the average of them.
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("X: ");
        float x = scanner.nextFloat();
        System.out.print("Y: ");
        float y = scanner.nextFloat();
        System.out.println();

        float average = (x + y) / 2;

        System.out.println("text");

        System.out.println("Average: " + average);
    }
}
