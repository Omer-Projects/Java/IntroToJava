package com.company.lesson1.e4;

import java.util.Scanner;

public class E4 {

    // Take two numbers (Integers) and print the average of them.
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("X: ");
        int x = scanner.nextInt();
        System.out.print("Y: ");
        int y = scanner.nextInt();
        System.out.println();

        int average = (x + y) / 2;

        System.out.println("Average: " + average);
    }
}
