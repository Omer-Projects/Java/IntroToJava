package com.company.lesson1.e5;

import java.util.Scanner;

public class E5 {

    // Take number and print his first char
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Number: ");
        int number = scanner.nextInt();

        System.out.println(number % 10);
    }
}
