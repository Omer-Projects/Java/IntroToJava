package com.company.lesson1.e6;

import java.util.Scanner;

public class E6 {

    // Take number and print his second char
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Number: ");
        int number = scanner.nextInt();

        System.out.println(number % 100 / 10);
    }
}
