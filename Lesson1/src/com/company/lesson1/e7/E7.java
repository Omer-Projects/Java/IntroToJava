package com.company.lesson1.e7;

import java.util.Scanner;

public class E7 {

    // Take number and print his chars in seperate lines.
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Number: ");
        int number = scanner.nextInt();

        System.out.println("0: " + (number % 10));
        System.out.println("1: " + (number % 100 / 10));
        System.out.println("2: " + (number / 10));
    }
}
