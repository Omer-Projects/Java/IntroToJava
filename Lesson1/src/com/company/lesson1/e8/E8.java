package com.company.lesson1.e8;

import java.util.Scanner;

public class E8 {

    //
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("X: ");
        int x = scanner.nextInt();
        System.out.print("Y: ");
        int y = scanner.nextInt();
        System.out.println();

        if (y == 0) {
            int result = x % y;
            System.out.println("Result: " + result);
        }
        else {
            System.out.println("[ERROR]: Y can't be zero!");
        }
    }
}
