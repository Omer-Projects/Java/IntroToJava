package com.company.lesson1.e9;

import java.util.Scanner;

public class E9 {

    // Take two numbers and swith them
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Number: ");
        int number = scanner.nextInt();

        int digit0 = number % 10;
        int digit1 = number % 100 / 10;
        int digit2 = number / 10;

        int result = digit0 + digit1 + digit2;

        System.out.println("Result: " + result);
    }
}
