package Lesson10;

public class BinaryTree<T> {

    public T m_Data;
    public BinaryTree<T> m_Left, m_Right;

    public BinaryTree(T data) {
        this.m_Data = data;
        this.m_Left = null;
        this.m_Right = null;
    }

    public BinaryTree(T data, BinaryTree<T> left, BinaryTree<T> right) {
        this.m_Data = data;
        this.m_Left = left;
        this.m_Right = right;
    }

    public int getHeight() {
        int leftHeight = 0;
        if (m_Left != null) {
            leftHeight = m_Left.getHeight();
        }

        int rightHeight = 0;
        if (m_Right != null) {
            rightHeight = m_Right.getHeight();
        }

        return Math.max(leftHeight, rightHeight) + 1;
    }
}
