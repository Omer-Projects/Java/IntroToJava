package Lesson10;

import java.util.Locale;

public class Flower {

    public String name;
    public String color;
    public int petals;

    public Flower() {
        this.name = "";
        this.color = "red";
        this.petals = 0;
    }

    public Flower(String name, String color, int petals) {
        this.name = name;
        this.color = color;
        this.petals = petals;
    }

    public String toString() {
        return this.color + " " + name;
    }

    public void rename(String newName) {
        if (!newName.equals("") && newName.length() < 10) {
            this.name = newName;
        }
    }

    public void removePetal() {
        if (this.petals > 0) {
            this.petals--;
        } else {
            System.out.println("\"" + this.name + "\" say: Leave me alone!");
        }
    }

    public Flower clone(String name) {
        return new Flower(name, this.color, this.petals);
    }

    public Flower[] clones(String[] names) {

        Flower[] flowers = new Flower[names.length];

        for (int i = 0; i < flowers.length; i++) {
            String name = names[i];
            flowers[i] = this.clone(name);
        }

        return flowers;
    }
}