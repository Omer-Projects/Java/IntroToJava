package Lesson10;

public class Node<T> {

    public T m_Data;
    public Node<T> m_Next;

    public Node(T data) {
        this.m_Data = data;
        this.m_Next = null;
    }

    public Node(T data, Node<T> parent) {
        this.m_Data = data;

        parent.m_Next = this;
    }

    public void addNode(T data) {
        addNode(new Node<T>(data));
    }

    public void addNode(Node<T> newNode) {
        Node<T> node = this;

        while (node.m_Next != null) {
            node = node.m_Next;
        }

        // Add node
        node.m_Next = newNode;
    }

    public void removeNode(Node<T> removedNode) {
        Node<T> node = this;

        while (node.m_Next != removedNode && node.m_Next != null) {
            node = node.m_Next;
        }

        if (node.m_Next != null) {
            // Remove node
            node.m_Next = node.m_Next.m_Next;
        }
    }

    public int getLength() {
        int length = 1;
        Node<T> node = this;
        while (node.m_Next != null) {
            node = node.m_Next;
            length++;
        }

        return length;
    }
}