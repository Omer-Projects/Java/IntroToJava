package Lesson10;

public class Point {

    public double x;
    public double y;

    public Point() {
        this.x = 0;
        this.y = 0;
    }

    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Point(int x) {
        this.x = x;
        this.y = this.x * this.x;
    }

    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }
}
