package Lesson11;

public class ST {
    public String[] m_Values;
    public int m_Index = 0;

    public ST(String st) {
        m_Values = st.split("\\s");
    }

    public boolean hasMoreElements() {
        return m_Index < m_Values.length;
    }

    public String nextElement() {
        m_Index++;
        return m_Values[m_Index - 1];
    }
}
