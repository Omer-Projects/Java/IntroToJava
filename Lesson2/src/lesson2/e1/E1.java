package lesson2.e1;

import java.util.Scanner;

public class E1 {

    // Take two numbers and solve "linarit" equation
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("a: ");
        float a = scanner.nextFloat();
        System.out.print("b: ");
        float b = scanner.nextFloat();

        float x;

        if (a == 0) {

            if (b == 0) {

                System.out.println("X can be any number!");
                return;
            }
            else {

                System.out.println("Dos not exit X that can sole the equation!");
                return;
            }
        }
        else {

            x = -b / a;
        }
        System.out.println(a + "x + " + b + " = 0");
        System.out.println("X: " + x);
    }
}
