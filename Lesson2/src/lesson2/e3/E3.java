package lesson2.e3;

import java.util.Scanner;

public class E3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("n: ");
        int n = scanner.nextInt();
        int x = n;
        int count = 0;

        if (x == 0)
            count++;
        else {
            while (x > 0) {
                x /= 10;
                count++;
            }
        }

        System.out.println(n + " has " + count + " chapters");
    }
}
