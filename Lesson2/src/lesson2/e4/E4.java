package lesson2.e4;

import java.util.Scanner;

public class E4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("n: ");
        int n = scanner.nextInt();
        int count = 0;

        while (n > 0) {
            if (n % 10 == 0) {
                count++;
            }
            n /= 10;
        }

        System.out.println("Has " + count + " chapter of 'zero'.");
    }
}
