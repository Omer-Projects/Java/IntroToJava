package lesson2.e5;

import java.util.Scanner;

public class E5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("n: ");
        int n = scanner.nextInt();
        int sum = 1;

        for (int i = 2; i <= n; i++) {
            sum *= i;
        }

        System.out.println("!" + n + " = " + sum);
    }
}
