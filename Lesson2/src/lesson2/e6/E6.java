package lesson2.e6;

import java.util.Scanner;

public class E6 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("m: ");
        int m = scanner.nextInt();
        System.out.print("n: ");
        int n = scanner.nextInt();
        int sum = 1;

        for (int i = 0; i < n; i++) {
            sum *= n;
        }

        System.out.println(m + " ** " + n + " = " + sum);
    }
}
