package lesson2.e7;

import java.util.Scanner;

public class E7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("m: ");
        int m = scanner.nextInt();
        System.out.print("n: ");
        int n = scanner.nextInt();

        if (n > m)
        {
            n = n + m;
            m = n - m;
            n = n - m;
        }

        for (int i = n; i > 0; i--) {

            if (n % i == 0 & m % i == 0) {
                System.out.println("result = " + i);
                return;
            }
        }
    }
}
