package lesson2.e8;

import java.util.Scanner;

public class E8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int min = Integer.MAX_VALUE;
        for (int i = 0; i < 3; i++)
        {
            System.out.print("Number: ");
            int num = scanner.nextInt();
            if (num < min)
                min  = num;
        }

        System.out.println("Min = " + min);
    }
}
