package lesson2.e9;

import java.util.Scanner;

public class E9 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int max = Integer.MIN_VALUE;
        for (int i = 0; i < 3; i++)
        {
            System.out.print("Number: ");
            int num = scanner.nextInt();
            if (num > max)
                max  = num;
        }

        System.out.println("Max = " + max);
    }
}
