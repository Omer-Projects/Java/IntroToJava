package lesson3;

import java.util.Scanner;

import lesson3.e1.E1;
import lesson3.e2.E2;
import lesson3.e3.E3;
import lesson3.e4.E4;
import lesson3.e5.E5;
import lesson3.e6.E6;
import lesson3.e7.E7;
import lesson3.e8.E8;

public class Main {

    /*
     * Lesson 3
     *
     * Select and run the exercises.
     * */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Choose a exercise: ");
        int exerciseNumber = scanner.nextInt();
        System.out.println();

        switch (exerciseNumber)
        {
            case 1: E1.main(args); break;
            case 2: E2.main(args); break;
            case 3: E3.main(args); break;
            case 4: E4.main(args); break;
            case 5: E5.main(args); break;
            case 6: E6.main(args); break;
            case 7: E7.main(args); break;
            case 8: E8.main(args); break;
        }
    }
}
