package lesson3.e3;

import java.util.Scanner;

public class E3 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("N: ");
        int n = scanner.nextInt();

        System.out.print("M: ");
        int m = scanner.nextInt();

        int result = 1;
        for (int i = 0; i < m; i++) {
            result *= n;
        }

        System.out.println(n + " ** " + m + " = " + result);
    }
}
