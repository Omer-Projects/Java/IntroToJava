package lesson3.e4;

import java.util.Scanner;

public class E4 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("N: ");
        int n = scanner.nextInt();

        int a = 1, b = 1;
        for (int i = 2; i < n; i++) {
            if (i % 2 == 0)
            {
                a += b;
            }
            else
            {
                b += a;
            }
        }

        System.out.println("Fib(" + n + ") = " + ((a > b) ? a : b));
    }
}
