package lesson3.e5;

import java.util.Scanner;

public class E5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("N: ");
        int n = scanner.nextInt();

        int max = scanner.nextInt();

        for (int i = 2; i < n; i++) {
            int num = scanner.nextInt();

            if (num > max)
            {
                max = num;
            }
        }

        System.out.println("Max =  " + max);
    }
}
