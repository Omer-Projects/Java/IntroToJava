package lesson3.e6;

import java.util.Scanner;

public class E6 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("N: ");
        int n = scanner.nextInt();

        int sum = 1;
        for (int i = 2; i < n && sum <= n; i++) {

            if (n % i == 0) {
                sum += i;
            }
        }

        if (sum == n)
        {
            System.out.println("Is perfect number");
        }
        else
        {
            System.out.println("Is not perfect number");
        }
    }
}
