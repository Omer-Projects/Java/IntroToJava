package lesson3.e8;

import java.util.ArrayList;
import java.util.Scanner;

public class E8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        ArrayList<Long> firstNumbers = new ArrayList<>();
        firstNumbers.add(2l);
        firstNumbers.add(3l);
        firstNumbers.add(5l);

        long num = 6;
        while (num < 100) {

            boolean isFirstNumber = true;

            long sqrtNum = (int)Math.sqrt(num) + 1;
            for (int i = 0; i < firstNumbers.size() && isFirstNumber; i++) {
                long check = firstNumbers.get(i);

                if (check > sqrtNum)
                {
                    break;
                }

                if (num % check == 0)
                {
                    isFirstNumber = false;
                }
            }

            if (isFirstNumber)
            {
                firstNumbers.add(num);
                System.out.println(num);
            }
            num++;
        }
    }
}
