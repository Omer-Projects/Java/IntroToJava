package lesson4;

import java.util.Scanner;

public class EX1_A {

    /*
    * Take two numbers and print all the Prime Gap couples.
    *
    * 207964859 - Omer Priel
    */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Prime Gap");
        System.out.print("First Number: ");
        int n = scanner.nextInt();
        System.out.print("Second Number: ");
        int m = scanner.nextInt();

        if (n <= 0 || m < 0) {
            System.out.println("Give positve numbers!");
            return;
        }

        if (n <= m) {
            System.out.println("The First Number must be bigger the the Secound Number!");
            return;
        }

        for (int x = 2; x + m <= n; x++)
        {
            boolean isPrimeGap = true;
            int y = x + m;

            // check if x and y a Prime Gap
            for (int i = 2; i < x && isPrimeGap; i++)
            {
                isPrimeGap = (x % i != 0) && (y % i != 0);
            }

            for (int i = x; i < y && isPrimeGap; i++)
            {
                isPrimeGap = (y % i != 0);
            }

            if (isPrimeGap)
            {
                System.out.println("(" + x + ", " + y + ")");
            }
        }
    }
}
