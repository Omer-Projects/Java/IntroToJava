package lesson4;

import java.util.Scanner;

public class EX1_B {

    /*
     * Solve a Quadratic Equation.
     *
     * 207964859 - Omer Priel
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        boolean run = true;

        System.out.println("Quadratic Equation:");
        while (run) {

            // Quadratic Equation
            System.out.println("ax^2 + bx + c = 0");
            System.out.print("a: ");
            double a = scanner.nextFloat();
            System.out.print("b: ");
            double b = scanner.nextFloat();
            System.out.print("c: ");
            double c = scanner.nextFloat();

            System.out.println(a + "x^2 + " + b + "x + " + c + " = 0");

            if (a == 0) {
                if (b == 0) {
                    if (c == 0) {
                        System.out.println("Any x");
                    } else {
                        System.out.println("No solution");
                    }
                } else {
                    double x = (-c / b);
                    System.out.println("X = " + x);
                }
            } else {
                double delta = b*b - 4*a*c;
                if (delta < 0)
                {
                    System.out.println("No solution");
                } else if (delta == 0) {
                    double x = (-b / 2*a);
                    System.out.println("X = " + x);
                } else {
                    double x1 = ((-b - Math.sqrt(delta)) / 2*a);
                    double x2 = ((-b + Math.sqrt(delta)) / 2*a);

                    System.out.println("X = " + x1 + ", X = " + x2);
                }
            }

            // Next Frame
            System.out.println("Main Menu:");
            System.out.println("0 - Exit:");
            System.out.println("1 - Solve Quadratic Equation:");
            System.out.print("Choose: ");
            run = scanner.nextInt() != 0;
        }
    }
}
