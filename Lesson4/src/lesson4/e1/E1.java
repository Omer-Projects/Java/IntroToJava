package lesson4.e1;

import java.util.Random;
import java.util.Scanner;

public class E1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.print("N: ");
        int n = scanner.nextInt();

        int[] arr = new int[n];
        int count = 0;

        for (int i = 0; i < n; i++) {
            arr[i] = random.nextInt(n);
            if (arr[i] == 0) {
                count++;
            }
        }

        System.out.println("count = " + count);
    }
}
