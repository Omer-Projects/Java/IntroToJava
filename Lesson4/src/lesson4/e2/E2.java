package lesson4.e2;

import java.util.Random;
import java.util.Scanner;

public class E2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.print("N: ");
        int n = scanner.nextInt();

        int[] arr = new int[n];

        arr[0] = random.nextInt(n);
        int min = arr[0];
        int max = arr[0];

        for (int i = 1; i < n; i++) {
            arr[i] = random.nextInt(n);
            if (arr[i] < min) {
                min = arr[i];
            } else if (arr[i] > max) {
                max = arr[i];
            }
        }

        System.out.println("min = " + min);
        System.out.println("max = " + max);
    }
}
