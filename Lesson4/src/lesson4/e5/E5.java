package lesson4.e5;

import java.util.Scanner;

public class E5 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[] arr = {1, 2, 3, 4, 5, 4, 3, 2, 1};

        boolean isSim = true;
        for (int i = 0; i < arr.length / 2 & isSim; i++) {
            isSim = arr[i] == arr[arr.length - 1 - i];
        }

        System.out.println((isSim) ? "Is Sim" : "Is not Sim");
    }
}