package lesson4.e6;

import java.util.Random;
import java.util.Scanner;

public class E6 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        System.out.print("N: ");
        int n = scanner.nextInt();

        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = random.nextInt(n);
            if (arr[i] % 3 == 0) {
                System.out.println(arr[i]);
            }
        }
    }
}
