package lesson4.e7;

import java.util.Random;
import java.util.Scanner;

public class E7 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        float arr[] = new float[3];

        for (int i = 0; i < arr.length; i++) {
            System.out.print("Give Number: ");
            arr[i] = scanner.nextFloat();
        }

        for (int i = 1; i < arr.length; i++) {
            float value = arr[i];
            for (int j = i - 1; j >= 0 && value < arr[j]; j--) {
                float temp = arr[j + 1];
                arr[j + 1] = arr[j];
                arr[j] = temp;
            }
        }

        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }
}
