package lesson4.e8;

import java.util.ArrayList;
import java.util.Scanner;

public class E8 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("N: ");
        int n = scanner.nextInt();

        int count = 0;

        while (n > 0) {
            count += n % 10;
            n /= 10;
        }

        System.out.println(count);
    }
}
