package lesson5;

import java.util.Scanner;

public class Main {

    /*
     * Lesson 5
     *
     * Select and run the exercises.
     * */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.print("Choose a exercise: ");
        int exerciseNumber = scanner.nextInt();
        System.out.println();

        switch (exerciseNumber)
        {
            case 1: lesson5.p1.P1.main(args); break;
            case 2: lesson5.p2.P2.main(args); break;
            case 3: lesson5.p3.P3.main(args); break;
        }
    }
}
