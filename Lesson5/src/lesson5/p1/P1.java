package lesson5.p1;

import java.util.Random;
import java.util.Scanner;

public class P1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();

        int[] arr = new int[20];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = random.nextInt(9) + 1;
        }

        for (int value : arr) {
            System.out.print(value + " ");
        }

        System.out.println();

        evenOddSort(arr);

        for (int value : arr) {
            System.out.print(value + " ");
        }
    }

    public static double arraySum(int [] arr) {
        int sum = 0;

        for (int value: arr) {
            sum += value;
        }

        return sum;
    }

    public static double arrayMean(int [] arr) {
        return arraySum(arr) / arr.length;
    }

    public static double arrayStdev(int [] arr) {
        double sum = 0;

        double mean = arrayMean(arr);

        for (int value: arr) {
            sum += value - mean;
        }

        return sum / arr.length;
    }

    public static boolean palindrom(int [] arr) {

        for (int i = 0; i < arr.length / 2; i++) {
            if (arr[i] != arr[arr.length - 1 - i]) {
                return false;
            }
        }

        return true;
    }

    public static void reverse(int [] arr) {
        for (int i = 0; i < arr.length / 2; i++) {
            int t = arr[i];
            arr[i] = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = t;
        }
    }

    public static void swap(int i, int j, int [] arr) {
        if (i < arr.length && j < arr.length) {
            int t = arr[i];
            arr[i] = arr[j];
            arr[j] = t;
        }
    }

    public static void evenOddSort(int [] arr) {

        int i = 0;
        int j = arr.length - 1;
        while (i + 1 < j) {

            if (arr[i] % 2 != 0) {
                while (arr[j] % 2 != 0) {
                    j--;
                }

                if (i < j) {
                    swap(i, j, arr);
                } else {
                    break;
                }
            }
            i++;
        }
    }
}
