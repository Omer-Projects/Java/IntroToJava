package lesson5.p2;

import java.util.Random;
import java.util.Scanner;

public class P2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int[][] arr = new int[][] {
                {1,3},
                {1,2, 5},
                {1,2,5, 5, 7}
        };

        prArr2(arr);

        prArr2(transpose(arr));
    }

    public static void prArr2(int[][] arr2) {

        for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr2[i].length; j++) {
                System.out.print(arr2[i][j] + " ");
            }
            System.out.println();
        }
    }

    public static int sumArr2(int [][]arr2) {

        int sum = 0;

        for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr2[i].length; j++) {
                sum += arr2[i][j];
            }
        }

        return sum;
    }

    public static int trace(int [][] arr2) {
        int sum = 0;

        for (int i = 0; i < arr2.length; i++) {
            sum += arr2[i][i];
        }

        return sum;
    }

    public static int trace2(int [][] arr2) {
        int sum = 0;

        for (int i = 0; i < arr2.length; i++) {
            sum += arr2[(arr2.length - 1) - i][i];
        }

        return sum;
    }

    public static boolean checkSymmetry(int [][] arr2) {
        for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr2.length; j++) {
                if (arr2[i][j] != arr2[arr2.length - 1 - i][arr2.length - 1 - j])
                    return false;
            }
        }
        return true;
    }

    public static int [][] transpose(int [][] arr2) {
        int[][] ret = new int[arr2.length][];

        for (int i = 0; i < arr2.length; i++) {
            ret[i] = new int[arr2[i].length];
            for (int j = 0; j < arr2[i].length; j++) {
                ret[i][j] = arr2[i][j];
            }
        }

        return ret;
    }

    public static int numZeros(int [][] arr2) {
        int count = 0;

        for (int i = 0; i < arr2.length; i++) {
            for (int j = 0; j < arr2[i].length; j++) {
                if (arr2[i][j] == 0) {
                    count++;
                }
            }
        }

        return count;
    }
}
