package lesson5.p3;

import java.util.Random;
import java.util.Scanner;

public class P3 {

    private static Scanner scanner = null;

    public static void main(String[] args) {
        scanner = new Scanner(System.in);

        System.out.print("Choose a question: ");
        int exerciseNumber = scanner.nextInt();
        System.out.println();

        switch (exerciseNumber)
        {
            case 1: q1(); break;
            case 2: q2(); break;
            case 3: q3(); break;
            case 4: q4(); break;
            case 6: q6(); break;
            case 7: q7(); break;
            case 8: q8(); break;
        }
    }

    private static String GetString(String label) {
        System.out.print(label + ": ");
        return scanner.next();
    }

    public static void q1() {

        String str1 = GetString("Text1");
        String str2 = GetString("Text2");

        System.out.println((str1.compareTo(str2) == 0) ? "The strings are equal" : "The strings are not equal");
    }

    public static void q2() {
        String str = GetString("Text");

        boolean simetry = true;
        for (int i = 0; simetry && i < str.length() / 2; i++) {
             simetry = (str.charAt(i) != str.charAt(str.length() - 1 - i));
        }

        System.out.println(simetry);
    }

    public static void q3() {

        String str = GetString("Text");

        String ret = "";
        for (int i = 0; i < str.length(); i++) {
            ret += str.charAt(str.length() - 1 - i);
        }

        System.out.println(ret);
    }

    public static void q4() {
        String str = GetString("Text");

        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == str.charAt(0));
        }

        System.out.println(count);
    }

    public static void q6() {
        String str = GetString("Text");

        String ret = "";
        while (str.length() > 0) {
            int index = 0;

            for (int i = 1; i < str.length(); i++) {
                if (str.charAt(index) > str.charAt(i)) {
                    index = i;
                }
            }

            ret += str.charAt(index);

            str = str.substring(0, index) + str.substring(index + 1);
        }

        System.out.println(ret);
    }

    public static void q7() {
        String str = GetString("Text");

        char tv = str.charAt(0);

        if ('a' <= tv && tv <= 'z') {
            tv = (char)((int)tv - (int)'a' + (int)'A');
        }

        String ret = tv + str.substring(1);

        System.out.println(ret);
    }

    public static void q8() {
        String str = GetString("Text");

        char max = str.charAt(0);
        for (int i = 1; i < str.length(); i++) {
            if (str.charAt(i) > max) {
                max = str.charAt(i);
            }
        }

        System.out.println(max);
    }
}
