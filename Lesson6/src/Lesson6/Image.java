package Lesson6;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.net.URL;

public class Image {

    // Members
    private Pixel[][] m_Pixels;

    // Constructors
    public Image(int wight, int height) {
        this.m_Pixels = new Pixel[height][wight];
    }

    public Image(String path) {
        BufferedImage res = null;

        try {
            res = ImageIO.read(new URL(path));
        }
        catch (Exception ex) {
            try {
                res = ImageIO.read(new File(path));
            }
            catch (Exception ex2) {}
        }


    }

    // Getters
    public Pixel getPixel(int x, int y) {
        return this.m_Pixels[y][x];
    }

    // Setters
    public void setPixel(int x, int y, int r, int g, int b) {
        this.m_Pixels[y][x].setColor(r, g, b);
    }

    public void setPixel(int x, int y, Pixel color) {
        this.m_Pixels[y][x].setColor(color);
    }

    // Actions
    public void Show() {

    }

    public void Save(String filepath) {

    }
}
