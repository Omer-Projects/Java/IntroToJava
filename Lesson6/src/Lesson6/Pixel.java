package Lesson6;

public class Pixel {
    // Members
    private int m_R;
    private int m_G;
    private int m_B;

    // Constructors
    public Pixel() {
        setColor(0, 0, 0);
    }

    public Pixel(int r, int g, int b) {
        setColor(r, g, b);
    }

    // Getters
    public int getR() {
        return this.m_R;
    }

    public int getG() {
        return this.m_G;
    }

    public int getB() {
        return this.m_B;
    }

    public void toArray(int[] arr) {
        arr[0] = this.m_R;
        arr[1] = this.m_G;
        arr[2] = this.m_B;
    }

    // Setters
    public void setR(int value) {
        this.m_R = value;
    }

    public void setG(int value) {
        this.m_G = value;
    }

    public void setB(int value) {
        this.m_B = value;
    }

    public void setColor(int r, int g, int b) {
        this.m_R = r;
        this.m_G = g;
        this.m_B = b;
    }

    public void setColor(Pixel color) {
        this.m_R = color.m_R;
        this.m_G = color.m_G;
        this.m_B = color.m_B;
    }
}