package Task2;

/**
 * This is a template for assignment 2 solution of introduction to compute
 * science course. Ariel University, 2021-2022.
 * 
 * Note: All the given 3D arrays (img) below represents a RGB image where img[0] - Red channel, 
 * img[1] - Green channel, img[2] - Blue channel,
 *  which can be acquired by readImageFromFile function in the attached file (MyImageIO.java).
 * 
 * @author Asaly Saed
 *  Good Luck and have a nice coding time :)!
 */

public class Ex2 {
	/**
	 * 
	 * @param img - an RGB image.
	 * @return a grayscale image of (im) as a 2D array.
	 */
	static public int[][] rgb2gray(int[][][] img) {

		int[][] ret = new int[img[0].length][img[0][0].length];

		for (int i = 0; i < ret.length; i++) {
			for (int j = 0; j < ret[0].length; j++) {
				int color = (int)Math.floor(0.3 * img[0][i][j] + 0.56 * img[1][i][j] + 0.11 * img[2][i][j]) * 255;

				ret[i][j] = color;
			}
		}

		return ret;
	}

	/**
	 * 
	 * @param img     - an RGB image.
	 * @param i_1,    j_1 - indicies of upper left pixel of the crop range.
	 * @param i_2,j_2 - indicies of lower right pixel of the crop range.
	 * @return an RGB cropped image starting from pixel (i_1, j_1) to pixel
	 *         (i_2,j_2) as 3D array.
	 */
	static public int[][][] crop(int[][][] img, int i_1, int j_1, int i_2, int j_2) {
		int width = j_2 - j_1 + 1;
		int height = i_2 - i_1 + 1;

		int[][][] ret = new int[3][height][width];

		for (int y = i_1; y < i_2; y++) {
			for (int x = j_1; x < j_2; x++) {
				for (int k = 0; k < 3; k++) {
					ret[k][y - i_1][x - j_1] = img[k][y][x];
				}
			}
		}


		return ret;
	}

	/**
	 * 
	 * @param img   - an RGB image.
	 * @param alpha - An angle we want to rotate the image with.
	 * @return a rotated image (img) by alpha degrees.
	 */
	static public int[][][] rotate_alpha(int[][][] img, double alpha) {
		int height = img[0].length;
		int width = img[0][0].length;

		int[][][] ret = new int[3][height][width];

		int centerX = width / 2;
		int centerY = height / 2;

		double cosA = Math.cos(Math.toRadians(360-alpha));
		double sinA = Math.sin(Math.toRadians(360-alpha));

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				for (int k = 0; k < 3; k++) {
					int x2 = (int)((cosA * (x - centerX) - sinA * (y - centerY))) + centerX;
					int y2 = (int)((sinA * (x - centerX) + cosA * (y - centerY))) + centerY;

					if (0 <= x2 && x2 < width && 0 <= y2 && y2 < height) {
						ret[k][y2][x2] = img[k][y][x];
					}
				}
			}
		}

		return ret;
	}

	/**
	 * 
	 * @param img - an RGB image.
	 * @param n   - the sliding window length to smooth the image by.
	 * @return a smoothed RGB image.
	 */
	static public int[][][] Smooth(int[][][] img, int n) {

		int height = img[0].length;
		int width = img[0][0].length;

		int[][][] ret = new int[3][height][width];

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				for (int k = 0; k < 3; k++) {

					int colorLevel = 0;

					int areaStartY = Math.max(y - n, 0);
					int areaStartX = Math.max(x - n, 0);
					int areaEndY = Math.min(y + n, height);
					int areaEndX = Math.min(x + n, width);

					for (int i = areaStartY; i < areaEndY; i++) {
						for (int j = areaStartX; j < areaEndX; j++) {
							colorLevel += img[k][i][j];
						}
					}

					int count = (areaEndY - areaStartY) * (areaEndX - areaStartX);
					ret[k][y][x] = colorLevel / count;
				}
			}
		}

		return ret;
	}

	/**
	 * 
	 * @param factor_h - height scale factor of the image.
	 * @param factor_w - width scale factor of the image.
	 * @param im       - a grayscale image to be scaled.
	 * @return a scaled up\down image by the given factors.
	 */
	static public int[][] scaleImg(double factor_h, double factor_w, int[][] im) {
		int height = (int)(im.length * factor_h);
		int width = (int)(im[0].length * factor_w);

		int[][] ret = new int[height][width];

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
	
				int y2 = (int)Math.floor(y / factor_h);
				int x2 = (int)Math.floor(x / factor_w);

				ret[y][x] = im[y2][x2];
			}
		}

		return ret;
	}

	/**
	 * Like the image in the PDF File (ex2_intro2021.pdf).
	 *
	 * @param factor_h - height scale factor of the image.
	 * @param factor_w - width scale factor of the image.
	 * @param im       - an RGB image to be scaled.
	 * @return a scaled up\down image by the given factors.
	 */
	static public int[][][] scaleImg(double factor_h, double factor_w, int[][][] im) {
		int height = (int)(im[0].length * factor_h);
		int width = (int)(im[0][0].length * factor_w);

		int[][][] ret = new int[3][height][width];

		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {

				int y2 = (int)Math.floor(y / factor_h);
				int x2 = (int)Math.floor(x / factor_w);

				for (int k = 0; k < 3; k++) {
					ret[k][y][x] = im[k][y2][x2];
				}
			}
		}

		return ret;
	}
}