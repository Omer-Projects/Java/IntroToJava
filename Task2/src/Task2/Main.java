package Task2;

import java.io.File;

public class Main {

    public static final String butterflyPath = "images\\butterfly.jpg";
    public static final String catPath = "images\\cat.jpg";
    public static final String outPath = "out\\";

    public static void main(String[] args) {

        test1("rgb2gray_b", butterflyPath);
        test1("rgb2gray_c", catPath);
        test2("crop_b", butterflyPath);
        test2("crop_c", catPath);
        test3("Smooth_b", butterflyPath);
        test3("Smooth_c", catPath);
        test4("rotate_alpha_b", butterflyPath);
        test4("rotate_alpha_c", catPath);
        test5("scaleImg_b", butterflyPath);
        test5("scaleImg_c", catPath);
        test6("scaleImgColored_b", butterflyPath);
        test6("scaleImgColored_c", catPath);
    }

    static public String getOutputPath(String testName) {
        return outPath + testName;
    }

    static public String getOutputPath(String testName, String imageName) {
        new File(outPath + testName).mkdir();
        return outPath + testName + "\\" + imageName;
    }

    static public void test1(String name, String fileName) {
        System.out.println(name);

        int[][] image = Task2.Ex2.rgb2gray(MyImageIO.readImageFromFile(fileName));

        MyImageIO.writeImageToFile(getOutputPath(name), image);
    }

    static public void test2(String name, String fileName) {
        int[][][] image = MyImageIO.readImageFromFile(fileName);

        int height = image[0].length / 10;
        int width = image[0][0].length / 10;

        for (int y = 0; y < 10; y++) {
            for (int x = 0; x < 10; x++) {
                System.out.println(name + y + "_" + x);
                int[][][] scaledImage = Task2.Ex2.crop(image, y * height,x * width,
                        Math.min(y * height + height, image[0].length), Math.min(x * width + width, image[0][0].length));
                MyImageIO.writeImageToFile(getOutputPath(name, + y + "_" + x), scaledImage);
            }
        }
    }

    static public void test3(String name, String fileName) {
        int[][][] image = MyImageIO.readImageFromFile(fileName);

        for (int i = 1; i <= 20; i++) {
            System.out.println(name + i);
            int[][][] smoothImage = Task2.Ex2.Smooth(image, i*5);
            MyImageIO.writeImageToFile(getOutputPath(name, i + ""), smoothImage);
        }
    }

    static public void test4(String name, String fileName) {
        int[][][] image = MyImageIO.readImageFromFile(fileName);

        for (int i = 0; i <= 36; i++) {
            System.out.println(name + i);
            int[][][] rotateImage = Task2.Ex2.rotate_alpha(image, i * 10);
            MyImageIO.writeImageToFile(getOutputPath(name, i + ""), rotateImage);
        }
    }

    static public void test5(String name, String fileName) {
        int[][] image = Task2.Ex2.rgb2gray(MyImageIO.readImageFromFile(fileName));

        image = Task2.Ex2.scaleImg(0.5, 0.5, image);

        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 5; j++) {
                System.out.println(name + i + "_" + j);
                int[][] scaledImage = Task2.Ex2.scaleImg(i, j, image);
                MyImageIO.writeImageToFile(getOutputPath(name,  i + "_" + j), scaledImage);
            }
        }
    }

    static public void test6(String name, String fileName) {
        int[][][] image = MyImageIO.readImageFromFile(fileName);

        image = Task2.Ex2.scaleImg(0.5, 0.5, image);

        for (int i = 1; i <= 5; i++) {
            for (int j = 1; j <= 5; j++) {
                System.out.println(name + i + "_" + j);
                int[][][] scaledImage = Task2.Ex2.scaleImg(i, j, image);
                MyImageIO.writeImageToFile(getOutputPath(name,  i + "_" + j), scaledImage);
            }
        }
    }
}
