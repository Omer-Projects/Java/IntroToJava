public class Ex3 {

    public static boolean validPassword(String password) {

        if (password.length() >= 6) {
            if (!password.contains(" ")) {

                boolean hasNumber = false;
                boolean hasSpecialCharacter = false;

                String specialCharacters = "!@#$%";

                for (int i = 0; i < password.length() && (!hasNumber || !hasSpecialCharacter); i++) {
                    char tv = password.charAt(i);
                    if ('0' <= tv && tv <= '9') {
                        hasNumber = true;
                    } else if (specialCharacters.contains(tv + "")) {
                        hasSpecialCharacter = true;
                    }
                }

                return hasNumber && hasSpecialCharacter;
            }
        }

        return false;
    }

    public static String sentence(String str) {

        StringBuilder ret = new StringBuilder("");
        String[] words = str.split("\\s+");

        for (int i = words.length - 1; i >= 0; i--) {
            ret.append(( i == words.length - 1 ? "" : " ") + words[i]);
        }

        return ret.toString();
    }

    public static boolean deletions(String s, String t) {

        if (s.length() < t.length()) {
            return false;
        }

        int j = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == t.charAt(j)) {
                j++;
                if (t.length() == j) {
                    return true;
                }
            }
        }

        return false;
    }

    public static int divide(int a, int b) {

        if (a < 0) {
            if (b < 0) {
                return divide(a * (-1), b * (-1));
            } else {
                return -1 * divide(a * (-1), b);
            }
        } else if (b < 0) {
            return -1 * divide(a, b  * (-1));
        }

        if (a < b) {
            return 0;
        }

        return divide(a - b, b) + 1;
    }
}
