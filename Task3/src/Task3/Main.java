package Task3;

public class Main {

    public static void main(String[] args) {

        System.out.println(validPassword("4!dsadadada")); // true
        System.out.println(validPassword("4!")); // false
        System.out.println(validPassword("4dsadadada")); // false
        System.out.println(validPassword("!dsadadada")); // false
        System.out.println(validPassword("4!ds adadada")); // false
        System.out.println(validPassword("aa@aa1aaa")); // true
        System.out.println(validPassword("aaa#aa2aa")); // true
        System.out.println(validPassword("aaaa$aa3a")); // true
        System.out.println(validPassword("aaaaa%a0a")); // true

        System.out.println(sentence("my name is omer"));
        System.out.println(sentence("my  name is omer priel"));
        System.out.println(sentence("what   is your name"));
        System.out.println(sentence(""));
        System.out.println(sentence("hello world"));
        System.out.println(sentence(sentence("hello world")));
        System.out.println(sentence("the quick brown fox"));
        System.out.println(sentence(sentence("the quick brown fox")));

        System.out.println(deletions("understand", "nerd")); // true
        System.out.println(deletions("team", "me")); // false
        System.out.println(deletions("ball", "handball")); // false

        System.out.println(divide(5,3));
        System.out.println(divide(6,2));
        System.out.println(divide(-25,5));
        System.out.println(divide(26,-5));
        System.out.println(divide(-49,-7));
    }

    public static boolean validPassword(String password) {

        if (password.length() >= 6) {
            if (!password.contains(" ")) {

                boolean hasNumber = false;
                boolean hasSpecialCharacter = false;

                String specialCharacters = "!@#$%";

                for (int i = 0; i < password.length() && (!hasNumber || !hasSpecialCharacter); i++) {
                    char tv = password.charAt(i);
                    if ('0' <= tv && tv <= '9') {
                        hasNumber = true;
                    } else if (specialCharacters.contains(tv + "")) {
                        hasSpecialCharacter = true;
                    }
                }

                return hasNumber && hasSpecialCharacter;
            }
        }

        return false;
    }

    public static String sentence(String str) {

        StringBuilder ret = new StringBuilder("");
        String[] words = str.split("\\s+");

        for (int i = words.length - 1; i >= 0; i--) {
            ret.append(( i == words.length - 1 ? "" : " ") + words[i]);
        }

        return ret.toString();
    }

    public static boolean deletions(String s, String t) {

        if (s.length() < t.length()) {
            return false;
        }

        int j = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == t.charAt(j)) {
                j++;
                if (t.length() == j) {
                    return true;
                }
            }
        }

        return false;
    }

    public static int divide(int a, int b) {

        if (a < 0) {
            if (b < 0) {
                return divide(a * (-1), b * (-1));
            } else {
                return -1 * divide(a * (-1), b);
            }
        } else if (b < 0) {
            return -1 * divide(a, b  * (-1));
        }

        if (a < b) {
            return 0;
        }

        return divide(a - b, b) + 1;
    }
}
