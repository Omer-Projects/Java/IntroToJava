package GameEx_Final;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.awt.*;

/**
 * This class represents a 2D circle in the plane. Please make sure you update it 
 * according to the GeoShape interface.
 * Ex4: you should update this class!
 * @author Asaly.Saed
 *
 */
public class Circle implements GeoShape{

	private int m_X, m_Y;
	private int m_Radius;
	private int m_DX = 1, m_DY = 1;
	private int m_ColorR, m_ColorG, m_ColorB;

	public Circle(int x, int y, int radius, int colorR, int colorG, int colorB) {
		m_X = x;
		m_Y = y;
		m_Radius = radius;
		m_ColorR = colorR;
		m_ColorG = colorG;
		m_ColorB = colorB;
	}

	/* 
	 * Please, do not change this function!
	 * 
	 */

	@Override
	public void draw(Graphics g, Component c) {
		g.setColor(new Color( getRed(),getGreen(),getBlue()));
		g.fillOval(getX(), getY(), getRadius(), getRadius());
		g.setColor(new Color(0,0,0));
		g.drawOval(getX(), getY(), getRadius(), getRadius());
	}

	@Override
	public void translateX(int dx) {
		m_X += dx;
	}

	@Override
	public void translateY(int dy) {
		m_Y += dy;
	}

	@Override
	public void setColor(int r, int g, int b) {
		m_ColorR = r;
		m_ColorG = g;
		m_ColorB = b;
	}

	@Override
	public boolean intersects(GeoShape g) {
		if (g instanceof Circle) {
			Circle circle = (Circle)g;

			double distance = Math.sqrt(Math.pow(m_X - circle.m_X, 2) + Math.pow(m_Y - circle.m_Y, 2));

			if (distance <= m_Radius + circle.m_Radius) { // Play Sound
				try {
					java.io.File soundFile = new java.io.File("sounds\\s1.wav");
					Clip clip = AudioSystem.getClip();
					clip.open(AudioSystem.getAudioInputStream(soundFile));
					clip.start();
				} catch (Exception e) {
					System.out.println(e);
				}
			}

			return (distance <= m_Radius + circle.m_Radius);
		} else if (g instanceof Rectangle){
			Rectangle rectangle = (Rectangle)g;

			boolean betweenX = (rectangle.getX() - m_Radius <= m_X) && (m_X <= rectangle.getX() + rectangle.getWidth() + m_Radius);
			boolean betweenY = (rectangle.getY() - m_Radius <= m_Y) && (m_Y <= rectangle.getY() + rectangle.getHeight() + m_Radius);

			if (betweenX && betweenY && rectangle.getHeight() > 10) { // Play Sound
				try {
					java.io.File soundFile = new java.io.File("sounds\\s2.wav");
					Clip clip = AudioSystem.getClip();
					clip.open(AudioSystem.getAudioInputStream(soundFile));
					clip.start();
				} catch (Exception e) {
					System.out.println(e);
				}
			}

			return betweenX && betweenY;
		}
		return false;
	}

	@Override
	public int getDx() {
		return m_DX;
	}

	@Override
	public void setDx(int dx) {
		m_DX = dx;
	}

	@Override
	public int getDy() {
		return m_DY;
	}

	@Override
	public void setDy(int dy) {
		m_DY = dy;
	}

	@Override
	public int getX() {
		return m_X;
	}

	@Override
	public void setX(int x) {
		m_X = x;
	}

	@Override
	public int getY() {
		return m_Y;
	}

	@Override
	public void setY(int y) {
		m_Y = y;
	}

	@Override
	public int getRed() {
		return m_ColorR;
	}

	@Override
	public int getGreen() {
		return m_ColorG;
	}

	@Override
	public int getBlue() {
		return m_ColorB;
	}

	public int getRadius() {
		return m_Radius;
	}

	public void setRadius(int radius) {
		m_Radius = radius;
	}
}
