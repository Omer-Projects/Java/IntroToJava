package GameEx_Final;

/**
 * This is a simple LinkedList class that implements LinkedListInterface.
 * Ex4: you should implement this class!
 * @author Asaly.Saed
 *
 */
public class LinkedList implements LinkedListInterface{

	private Node m_Head;

	public LinkedList() {
		m_Head = null;
	}

	@Override
	public Node getHead() {
		return m_Head;
	}

	@Override
	public void add(NodeInterface p) {
		if (p == null) {
			return;
		}

		if (m_Head == null) {
			m_Head = (Node)p;
		} else {
			Node node = m_Head;
			while (node.getNext() != null) {
				node = node.getNext();
			}
			node.setNext(p);
		}
	}

	@Override
	public void remove(NodeInterface p) {
		if (p == null) {
			return;
		}

		if (p == m_Head) {
			m_Head = m_Head.getNext();
		} else {
			Node node = m_Head;
			while (node.getNext() != p && node.getNext() != null) {
				node = node.getNext();
			}
			if (node.getNext() == p) {
				p.getData().setX(0);
				p.getData().setY(0);
				node.setNext(p.getNext());
			}
		}
	}
}
