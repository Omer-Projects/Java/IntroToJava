package GameEx_Final;
/**
 * This class represents a single node in a LinkedList that implements LinkedListInterface.
 * @author Asaly.Saed
 *
 */
public class Node implements NodeInterface{

	private GeoShape m_Data;
	private Node m_Next;

	public Node(GeoShape data, NodeInterface next) {
		m_Data = data;
		m_Next = (Node)next;
	}

	@Override
	public GeoShape getData() {
		return m_Data;
	}

	@Override
	public void setData(GeoShape g) {
		m_Data = g;
	}

	@Override
	public Node getNext() {
		return m_Next;
	}

	@Override
	public void setNext(NodeInterface next) {
		m_Next = (Node)next;
	}
}