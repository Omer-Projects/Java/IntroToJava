package GameEx_Final;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
/**
 * This class represents a 2D axis parallel rectangle.
 * Ex4: you should implement this class!
 * @author Asaly.Saed
 *
 */
public class Rectangle implements GeoShape {

	private int m_X, m_Y;
	private int m_Width, m_Height;
	private int m_DX = 1, m_DY = 1;
	private int m_ColorR, m_ColorG, m_ColorB;

	public Rectangle(int x, int y, int width, int height, int colorR, int colorG, int colorB) {
		m_X = x;
		m_Y = y;
		m_Width = width;
		m_Height = height;
		m_ColorR = colorR;
		m_ColorG = colorG;
		m_ColorB = colorB;
	}

	/* 
	 * Please, do not change this function!
	 * 
	 */
	public void draw(Graphics g, Component c) {
		g.setColor(new Color(getRed(), getGreen(), getBlue()));
		g.fillRect(getX(), getY(), getWidth(), getHeight());
		g.setColor(new Color(0, 0, 0));
		g.drawRect(getX(), getY(), getWidth(), getHeight());
	}

	@Override
	public void translateX(int dx) {
		m_X += dx;
	}

	@Override
	public void translateY(int dy) {
		m_Y += dy;
	}

	@Override
	public void setColor(int r, int g, int b) {
		m_ColorR = r;
		m_ColorG = g;
		m_ColorB = b;
	}

	@Override
	public boolean intersects(GeoShape g) {
		if (g instanceof Circle) {
			Circle circle = (Circle)g;

			return circle.intersects(this);
		} else if (g instanceof Rectangle){
			Rectangle rectangle = (Rectangle)g;

			boolean betweenX = !(m_X + m_Width <rectangle.m_X && rectangle.m_X + rectangle.m_Width < m_X);
			boolean betweenY = !(m_Y + m_Height <rectangle.m_Y && rectangle.m_Y + rectangle.m_Height < m_Y);
			return betweenX && betweenY;
		}
		return false;
	}

	@Override
	public int getDx() {
		return m_DX;
	}

	@Override
	public void setDx(int dx) {
		m_DX = dx;
	}

	@Override
	public int getDy() {
		return m_DY;
	}

	@Override
	public void setDy(int dy) {
		m_DY = dy;
	}

	@Override
	public int getX() {
		return m_X;
	}

	@Override
	public void setX(int x) {
		m_X = x;
	}

	@Override
	public int getY() {
		return m_Y;
	}

	@Override
	public void setY(int y) {
		m_Y = y;
	}

	@Override
	public int getRed() {
		return m_ColorR;
	}

	@Override
	public int getGreen() {
		return m_ColorG;
	}

	@Override
	public int getBlue() {
		return m_ColorB;
	}

	public int getWidth() {
		return m_Width;
	}

	public void setWidth(int width) {
		m_Width = width;
	}

	public int getHeight() {
		return m_Height;
	}

	public void setHeight(int height) {
		m_Height = height;
	}
}
