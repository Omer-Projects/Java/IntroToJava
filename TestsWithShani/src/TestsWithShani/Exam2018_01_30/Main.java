package TestsWithShani.Exam2018_01_30;

public class Main {

    public static void main(String[] args) {
        MyLinkedList list = new MyLinkedList();
        list.add("A");
        list.add("B");
        list.add("C");
        list.add("D");
        list.add("E");

        System.out.println(list);
        list.swap(0, 4);
        System.out.println(list);
        list.swap(0, 0);
        System.out.println(list);
        list.swap(1, 2);
        System.out.println(list);
    }

    public static String func(String s) {
        String ret = "";
        while (s.length() > 0) {
            ret += s.charAt(0);
            s = s.replaceAll(s.charAt(0) + "", "");
        }
        return ret;
    }

    // Q 1
    public static boolean square(int num) {
        int count = 0;
        for (int i = 0; i < num / 2; i++) {
            if (num % i == 0) {
                count += i * i;
                count += (num / i) * (num / i);
            }
        }
        int sq = (int)Math.sqrt(count);

        return sq * sq == count;
    }

    // Q 2
    public static String reduce(String s) {
        if (s.length() < 2) {
            return s;
        }

        String ret = "";
        ret += s.charAt(0);

        for (int i = 1; i < s.length(); i++) {
            if (s.charAt(i) != s.charAt(i-1)) {
                ret += s.charAt(i);
            }
        }

        return ret;
    }

    // Q 3
    public static boolean symmetric(int[][] arr1, int[][] arr2) {

        if (arr1.length != arr2.length) {
            return false;
        }

        boolean onX = true;

        for (int i = 0; onX && i < arr1.length / 2; i++) {
            onX = arr1[i].length == arr2[arr1.length - i - 1].length;
            for (int j = 0; onX && j < arr1[i].length; j++) {
                onX = arr1[i][j] == arr2[arr1.length - 1 - i][j];
            }
        }

        if (onX) {
            return true;
        }

        boolean onY = true;
        for (int i = 0; onY && i < arr1.length; i++) {
            onY = arr1[i].length == arr2[i].length;
            for (int j = 0; onY && j < arr1[i].length; j++) {
                onY = arr1[i][j] == arr2[i][arr1.length - 1 - j];
            }
        }

        return onY;
    }

    // Q 4 not needed

    // Q 5

}
