package TestsWithShani.Exam2018_01_30;

/*****************************************************************
*  @author Eyal Levi
*  קורס מבוא לחישוב - אוניברסיטת אריאל
*  https://github.com/LeviEyal
******************************************************************/

public class MyLinkedList {
    Node head;
    int size;

    public MyLinkedList() {
        head = null;
        size = 0;
    }

    public void add(String data) {
        if (head == null) {
            head = new Node(data);
        } else {
            Node n = head;
            while (n.next != null) {
                n = n.next;
            }
            n.next = new Node(data);
        }
        size++;
    }
    

    public String toString() {
        String ans = "[";
        if (head == null)
            ans = "[]";
        else {
            Node n = head;
            while (n.next != null) {
                ans = ans + n.data + ", ";
                n = n.next;
            }
            ans = ans + n.data + "]";
        }
        return ans;
    }

    public boolean swap(int i, int j){
        if (i < 0 || i >= size || j < 0 || j >= size) {
            return false;
        }

        if (i != j) {
            Node nodeIBack = null;
            Node nodeJBack = null;
            Node nodeI = null;
            Node nodeJ = null;

            Node node = head;
            for (int x = 0; nodeI == null || nodeJ == null; x++) {
                if (x == i - 1) {
                    nodeIBack = node;
                } else if (x == i) {
                    nodeI = node;
                }

                if (x == j - 1) {
                    nodeJBack = node;
                } else if (x == j) {
                    nodeJ = node;
                }

                node = node.next;
            }

            if (i == 0) {
                head = nodeJ;
            } else {
                nodeIBack.next = nodeJ;
            }

            if (j == 0) {
                head = nodeI;
            } else {
                nodeJBack.next = nodeI;
            }

            Node temp = nodeI.next;
            nodeI.next = nodeJ.next;
            nodeJ.next = temp;
        }

        return true;
    }
}