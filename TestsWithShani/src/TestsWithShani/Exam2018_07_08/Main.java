package TestsWithShani.Exam2018_07_08;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        String s = "a,a,a,a";
        System.out.println(s.split(",").length);

        int[][] arr = makeArray();
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j] + ",");
            }
            System.out.println();
        }
    }

    // Q 1
    public static boolean countExactly(int num, int digit, int count) {
        if (digit < 0 || digit > 9) {
            return false;
        }

        if (num == 0) {
            return digit == 0 && count == 1;
        }

        while (num >= 0 && count >= 0) {
            if (num % 10 == digit) {
                count--;
            }
            num /= 10;
        }

        return count == 0;
    }

    // Q 2

    // Q 3
    public static int[][] makeArray() {
        Scanner scanner = new Scanner(System.in);

        String input = scanner.next();

        String[] sp = input.split(",");

        int[][] arr = new int[sp.length][];

        for (int k = 0; k < arr.length; k++) {
            String[] params = sp[k].split("x");
            int value = Integer.parseInt(params[1]);
            arr[k] = new int[Integer.parseInt(params[0])];

            for (int i = 0; i < arr[k].length; i++) {
                arr[k][i] = value;
            }
        }

        return arr;
    }

    // Q 4
    // This is too much for me :)
}
