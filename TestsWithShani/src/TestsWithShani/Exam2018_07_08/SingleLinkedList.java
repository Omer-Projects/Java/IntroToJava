package TestsWithShani.Exam2018_07_08;

/*****************************************************************
*  @author Eyal Levi
*  קורס מבוא לחישוב - אוניברסיטת אריאל
*  https://github.com/LeviEyal
******************************************************************/

class Node {
    int data;
    Node next;

    public Node(int data) {
        this.data = data;
        next = null;
    }

    public Node(int data, Node next) {
        this.data = data;
        this.next = next;
    }

    public String toString() { 
        return "" + data;
    }
}

public class SingleLinkedList {
    Node head;
    int size;

    public SingleLinkedList() {
        head = null;
        size = 0;
    }

    public String toString() {
        if (head == null)
            return "[]";
        String res = "[";
        Node pointer = head;
        while (pointer.next != null) {
            res = res + pointer.toString() + ", ";
            pointer = pointer.next;
        }
        res = res + pointer.toString();
        return res + "]";
    }

    public void addInPlace(int num) {
        Node newNode = new Node(num);
        if (head == null) {
            head = newNode;

        } else if (head.data >= num) {
            newNode.next = head;
            head = newNode;

        } else {
            Node node = head;
            while (node.next != null && node.next.data < num) {
                node = node.next;
            }

            if (node.next != null) {
                newNode.next = node.next;
            }
            node.next = newNode;

        }

        size++;
    }
}