package TestsWithShani.Exam2018_08_01;

public class Main {

    public static void main(String[] args) {
        
    }

    // Q 1
    public static boolean isEvenFromTo(int[] arr, int from, int to) {
        if (from > to || from < 0 || to >= arr.length) {
            return false;
        }

        if (from == to) {
            return from % 2 == 0;
        }

        return to % 2 == 0 && isEvenFromTo(arr, from, to - 1);
    }

    // Q 2
    public static String removeByChance(String str, int n) {

        while (n > 0 && str.length() > 0) {
            int i = (int)(Math.random() * str.length() + 0);

            str = str.substring(0, i) + str.substring(i+1, str.length());

            n--;
        }
        return  str;
    }

    // Q 3
    public static void shiftRight(int[][] mat) {
        if (mat.length == 0 || mat[0].length < 2) {
            return;
        }

        for (int i = 0; i < mat.length; i++) {
            int len = mat[i].length;
            int last = mat[i][len - 1];

            for (int j = len - 1; j > 0; j--) {
                mat[i][j] = mat[i][j-1];
            }

            mat[i][0] = last;
        }
    }
}
