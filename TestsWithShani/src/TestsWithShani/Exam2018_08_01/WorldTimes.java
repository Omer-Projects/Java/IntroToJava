package TestsWithShani.Exam2018_08_01;

public class WorldTimes {

    private static int s_NumOfObjects = 0;

    public static int getNumOfObjects() {
        return s_NumOfObjects;
    }

    private Clock[] m_Times;

    public WorldTimes(int n) {
        m_Times = new Clock[n];

        for (int i = 0; i < n; i++) {
            m_Times[i] = new Clock(i, 0);
        }

        s_NumOfObjects++;
    }

    public WorldTimes(WorldTimes other) {
        m_Times = new Clock[other.m_Times.length];

        for (int i = 0; i < m_Times.length; i++) {
            m_Times[i] = new Clock(other.m_Times[i]);
        }

        s_NumOfObjects++;
    }

    public Clock getTimeIndex(int i) {
        if (0 <= i && i < m_Times.length) {
            return m_Times[i];
        }

        return null;
    }

    public void addOne() {
        for (int i = 0; i < m_Times.length; i++) {
            m_Times[i].setHours((m_Times[i].getHours()+1) % 24);
        }
    }
}

